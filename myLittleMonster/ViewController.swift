//
//  ViewController.swift
//  myLittleMonster
//
//  Created by Rong Wang on 2016-01-19.
//  Copyright © 2016 Rong Wang. All rights reserved.
//

import UIKit
import AVFoundation

class ViewController: UIViewController {
    
    @IBOutlet weak var monsterImg : MonsterImg!
    @IBOutlet weak var foodImg : dragImg!
    @IBOutlet weak var heartImg : dragImg!
    
    
    @IBOutlet weak var penalty1: UIImageView!
    @IBOutlet weak var penalty2: UIImageView!
    @IBOutlet weak var penalty3: UIImageView!
    
    @IBOutlet weak var PlayBtn: UIButton!
    
    @IBOutlet weak var replayBtn: UIButton!
    
    
    let DIM_ALPHA : CGFloat = 0.2
    let OPAQUE : CGFloat = 1.0
    let MAX_PENALTY : Int = 3
    var curPanalties = 0
    
    var timer:NSTimer!
    var monsterHappy = false
    var curItem : UInt32 = 0
    var musicPlayer : AVAudioPlayer!
    var sfxBite: AVAudioPlayer!
    var sfxHeart: AVAudioPlayer!
    var sfxDeath: AVAudioPlayer!
    var sfxSkull: AVAudioPlayer!

    override func viewDidLoad() {
        
        super.viewDidLoad()
        
        foodImg.hidden = true
        heartImg.hidden = true
        penalty1.hidden = true
        penalty2.hidden = true
        penalty3.hidden = true
        monsterImg.hidden = true
        replayBtn.hidden = true

        
        foodImg.dropTarget = monsterImg
        heartImg.dropTarget = monsterImg
        foodImg.userInteractionEnabled = false
        heartImg.userInteractionEnabled = false
        
        penalty1.alpha = DIM_ALPHA
        penalty2.alpha = DIM_ALPHA
        penalty3.alpha = DIM_ALPHA
        
        NSNotificationCenter.defaultCenter().addObserver(self, selector: "itemDroppedOnCharacter:", name: "onTargetDropped", object: nil)
        
        do {
            let resourcePath = NSBundle.mainBundle().pathForResource("cave-music", ofType: "mp3")!
            let url = NSURL(fileURLWithPath: resourcePath)
            try musicPlayer = AVAudioPlayer(contentsOfURL: url)
            
            try sfxBite = AVAudioPlayer(contentsOfURL: NSURL(fileURLWithPath: NSBundle.mainBundle().pathForResource("bite", ofType: "wav")!))
            
            try sfxHeart = AVAudioPlayer(contentsOfURL: NSURL(fileURLWithPath: NSBundle.mainBundle().pathForResource("heart", ofType: "wav")!))
            
            try sfxDeath = AVAudioPlayer(contentsOfURL: NSURL(fileURLWithPath: NSBundle.mainBundle().pathForResource("death", ofType: "wav")!))
            
            try sfxSkull = AVAudioPlayer(contentsOfURL: NSURL(fileURLWithPath: NSBundle.mainBundle().pathForResource("skull", ofType: "wav")!))
            
            musicPlayer.prepareToPlay()
            
            
            sfxBite.prepareToPlay()
            sfxHeart.prepareToPlay()
            sfxDeath.prepareToPlay()
            sfxSkull.prepareToPlay()
            
        } catch let err as NSError {
            print(err.debugDescription)
        }

        
        //startTimer()
        
 
    }
    
    
    @IBAction func OnPlayPressed(sender: AnyObject) {
        
        curPanalties = 0
        if timer != nil{
            timer.invalidate()
        }
        
        foodImg.hidden = false
        heartImg.hidden = false

        penalty1.hidden = false
        penalty2.hidden = false
        penalty3.hidden = false
        monsterImg.hidden = false
        replayBtn.hidden = true
        PlayBtn.hidden = true
        
        
        musicPlayer.play()
        startTimer()
        
        
    }
    
    
    @IBAction func OnReplayPressed(sender: AnyObject) {
        
        
        curPanalties = 0
        monsterHappy = false
        curItem = 0
        
        self.viewDidLoad()
        foodImg.hidden = false
        heartImg.hidden = false
        penalty1.hidden = false
        penalty2.hidden = false
        penalty3.hidden = false
        monsterImg.hidden = false
        replayBtn.hidden = true
        PlayBtn.hidden = true
        monsterImg.playIdleAnimation()
        musicPlayer.play()
        startTimer()
    }
    
    func itemDroppedOnCharacter(notif: AnyObject) {
        
        monsterHappy = true
        startTimer()
        
        foodImg.alpha = DIM_ALPHA
        foodImg.userInteractionEnabled = false
        heartImg.alpha = DIM_ALPHA
        heartImg.userInteractionEnabled = false
        
        if curItem == 0 {
            sfxHeart.play()
        } else {
            sfxBite.play()
        }
    }
    
    func startTimer(){
        if timer != nil{
            timer.invalidate()
        }
        //NSNotificationCenter.defaultCenter().addObserver(self, selector: "itemDroppedOnCharacter:", name: "onTargetDropped", object: nil)
        timer = NSTimer.scheduledTimerWithTimeInterval(3.0, target: self, selector: "changeGameState", userInfo: nil, repeats: true)
    }

   
    func changeGameState(){
        
        if !monsterHappy{
            curPanalties++
            
            sfxSkull.play()
            if curPanalties == 1{
                penalty1.alpha = OPAQUE
                penalty2.alpha = DIM_ALPHA
                //penalty3.alpha = DIM_ALPHA
            }else if curPanalties == 2{
            
                //penalty1.alpha = OPAQUE
                penalty2.alpha = OPAQUE
                penalty3.alpha = DIM_ALPHA
                
            }else if curPanalties >= 3{
                //penalty1.alpha = OPAQUE
                //penalty2.alpha = OPAQUE
                penalty3.alpha = OPAQUE
            }else{
                penalty1.alpha = DIM_ALPHA
                penalty2.alpha = DIM_ALPHA
                penalty3.alpha = DIM_ALPHA
            }
        
            if curPanalties >= MAX_PENALTY{
                gameOver()
                
                /*foodImg.hidden = true
                heartImg.hidden = true
                penalty1.hidden = true
                penalty2.hidden = true
                penalty3.hidden = true
                monsterImg.hidden = true*/
                replayBtn.hidden = false
                PlayBtn.hidden = true
                foodImg.hidden = true
                heartImg.hidden = true
                
                musicPlayer.stop()
            }
        }
        
        let rand = arc4random_uniform(2) // 0 or 1
        
        if rand == 0{
            foodImg.alpha = DIM_ALPHA
            foodImg.userInteractionEnabled = false
            heartImg.alpha = OPAQUE
            heartImg.userInteractionEnabled = true
        }else{
            foodImg.alpha = OPAQUE
            foodImg.userInteractionEnabled = true
            heartImg.alpha = DIM_ALPHA
            heartImg.userInteractionEnabled = false
        }
        
        curItem = rand
        monsterHappy = false
        
    }
    
    func gameOver(){
        timer.invalidate()
        monsterImg.playDeath()
        sfxDeath.play()
        
        /*foodImg.hidden = true
        heartImg.hidden = true
        penalty1.hidden = true
        penalty2.hidden = true
        penalty3.hidden = true
        monsterImg.hidden = true
        replayBtn.hidden = false
        PlayBtn.hidden = true
        
        musicPlayer.stop()*/
    }

}

